package edu.ntnu.idatt2001.mappe1.hospital;

import java.util.Objects;

public abstract class Person {
    private String firstName;
    private String lastName;
    private final String socialSecurityNumber;

    /**
     * Creates a new person.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if (firstName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("firstName cannot be null or blank");
        }
        if (lastName == null || lastName.isBlank()) {
            throw new IllegalArgumentException("lastName cannot be null or blank");
        }
        if (socialSecurityNumber == null || socialSecurityNumber.isBlank()) {
            throw new IllegalArgumentException("socialSecurityNumber cannot be null or blank");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the persons first name.
     * @param firstName cannot be blank or null.
     */
    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("firstName cannot be null or blank");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the persons last name.
     * @param lastName cannot be blank or null.
     */
    public void setLastName(String lastName) {
        if (lastName == null || lastName.isBlank()) {
            throw new IllegalArgumentException("firstName cannot be null or blank");
        }
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /*
    Social security number should not be settable, as it by definition
    identifies a person uniquely. Changing it should therefore likely
    create a new person. Having it mutable also causes problems when
    storing the persons, as they cannot be checked for duplicates, since
    the social security number can be set to another person's number.

    Check commit 9ead1c for previous implementation.
    */

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return socialSecurityNumber.equals(person.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
