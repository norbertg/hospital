package edu.ntnu.idatt2001.mappe1.hospital.employees;

import edu.ntnu.idatt2001.mappe1.hospital.Employee;
import edu.ntnu.idatt2001.mappe1.hospital.Patient;

public abstract class Doctor extends Employee {
    /**
     * Creates a new doctor.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
