package edu.ntnu.idatt2001.mappe1.hospital.employees;

import edu.ntnu.idatt2001.mappe1.hospital.Patient;

public class Surgeon extends Doctor {
    /**
     * Creates a new surgeon.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a patient's diagnosis.
     * @param patient A non-null patient who's diagnosis will be set.
     * @param diagnosis A non-null diagnosis description.
     *                  Use an empty string to represent no diagnosis.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        if (patient == null) {
            throw new IllegalArgumentException("patient cannot be null");
        }
        if (diagnosis == null) {
            throw new IllegalArgumentException("diagnosis cannot be null");
        }
        patient.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return "Surgeon{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
