package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.employees.GeneralPractitioner;
import edu.ntnu.idatt2001.mappe1.hospital.employees.Nurse;
import edu.ntnu.idatt2001.mappe1.hospital.employees.Surgeon;
import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;

public final class HospitalTestData {
    private HospitalTestData() {
        //notcalled
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) throws AddException {
        //Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("OddEven","Primtallet","14130244806"));
        emergency.addEmployee(new Employee("Huppasahn","DelFinito","39431555196"));
        emergency.addEmployee(new Employee("Rigmor","Mortis","79406867744"));
        emergency.addEmployee(new GeneralPractitioner("Inco","Gnito","59427526831"));
        emergency.addEmployee(new Surgeon("Inco","Gnito","44776706386"));
        emergency.addEmployee(new Nurse("Nina","Teknologi","34059602991"));
        emergency.addEmployee(new Nurse("Ove","Ralt","45285385719"));
        emergency.addPatient(new Patient("Inga","Lykke","64532961070"));
        emergency.addPatient(new Patient("Ulrik","Smål","44867229042"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barnpoliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti","Kaffen","10439666021"));
        childrenPolyclinic.addEmployee(new Employee("NidelV.","Elvefølger","11615568689"));
        childrenPolyclinic.addEmployee(new Employee("Anton","Nym","87738690810"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene","Sis","76243557653"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna","Na","14624934666"));
        childrenPolyclinic.addEmployee(new Nurse("Nora","Toriet","50222844283"));
        childrenPolyclinic.addPatient(new Patient("Hans","Omvar","22226841585"));
        childrenPolyclinic.addPatient(new Patient("Laila","La","55914741263"));
        childrenPolyclinic.addPatient(new Patient("Jøran","Drebli","47368813652"));
        hospital.addDepartment(childrenPolyclinic);
    }
}