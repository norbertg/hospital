package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;
import edu.ntnu.idatt2001.mappe1.hospital.exceptions.RemoveException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Department {
    private final String name;
    private final HashMap<String, Patient> patients = new HashMap<>();
    private final HashMap<String, Employee> employees = new HashMap<>();

    /**
     * Creates a new department.
     * @param name A name that is neither null nor blank.
     */
    public Department(String name) {
        if (name == null || name.isBlank()) {
            throw new IllegalArgumentException("name cannot be null or blank");
        }
        this.name = name;
    }

    /**
     * Creates a copy of this department with the specified name.
     * @param name The new name for the department. Cannot be blank or null.
     * @return Department with the specified name.
     */
    public Department setName(String name) {
        if (name == null || name.isBlank()) {
            throw new IllegalArgumentException("name cannot be null or blank");
        }
        Department newDepartment = new Department(name);
        newDepartment.patients.putAll(patients);
        newDepartment.employees.putAll(employees);
        return newDepartment;
    }

    public String getName() {
        return name;
    }

    /**
     * Adds a new employee to the department.
     * @param employee Non-null employee
     * @throws AddException An employee with an equal social security number is already in the department.
     */
    public void addEmployee(Employee employee) throws AddException {
        if (employee == null) {
            throw new IllegalArgumentException("employee cannot be null");
        }
        String socialSecurityNumber = employee.getSocialSecurityNumber();
        if (employees.containsKey(socialSecurityNumber)) {
            throw new AddException("employee exists already");
        }
        employees.put(socialSecurityNumber, employee);
    }

    public List<Employee> getEmployees() {
        return new ArrayList<>(employees.values());
    }

    /**
     * Adds a new patient to the department.
     * @param patient Non-null patient
     * @throws AddException A patient with an equal social security number is already in the department.
     */
    public void addPatient(Patient patient) throws AddException {
        if (patient == null) {
            throw new IllegalArgumentException("patient cannot be null");
        }
        String socialSecurityNumber = patient.getSocialSecurityNumber();
        if (employees.containsKey(socialSecurityNumber)) {
            throw new AddException("patient exists already");
        }
        patients.put(socialSecurityNumber, patient);
    }

    public List<Patient> getPatients() {
        return new ArrayList<>(patients.values());
    }

    /**
     * Removes a person from the department.
     * The person will be removed as patient, employee or both.
     * @param person The non-null person to be removed.
     * @throws RemoveException The person was neither a patient nor an employee within the department.
     */
    public void remove(Person person) throws RemoveException {
        if (person == null) {
            throw new IllegalArgumentException("person cannot be null");
        }
        String socialSecurityNumber = person.getSocialSecurityNumber();
        if (!employees.containsKey(socialSecurityNumber) && !patients.containsKey(socialSecurityNumber)) {
            throw new RemoveException("the person does not exist within the department");
        }
        employees.remove(socialSecurityNumber);
        patients.remove(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", patients=" + patients +
                ", employees=" + employees +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return name.equals(that.name) &&
                patients.equals(that.patients) &&
                employees.equals(that.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
