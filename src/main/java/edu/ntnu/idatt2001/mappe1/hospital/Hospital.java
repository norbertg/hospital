package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class Hospital {
    private final String name;
    private final HashMap<String, Department> departments = new HashMap<>();

    /**
     * Creates a new hospital.
     * @param name A name that is neither null nor blank.
     */
    public Hospital(String name) {
        if (name == null || name.isBlank()) {
            throw new IllegalArgumentException("name cannot be null or blank");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Adds a new department to the hospital.
     * @param department The non-null department to be added.
     * @throws AddException The department with the specified name exists already within the hospital.
     */
    public void addDepartment(Department department) throws AddException {
        if (department == null) {
            throw new IllegalArgumentException("department cannot be null");
        }
        String name = department.getName();
        if (departments.containsKey(name)) {
            throw new AddException("the department already exists");
        }
        departments.put(name, department);
    }

    public List<Department> getDepartments() {
        return new ArrayList<>(departments.values());
    }

    /**
     * Gets a department by name.
     * @param name Name of the department.
     * @return Department if it exists, else, an empty optional.
     */
    public Optional<Department> getDepartment(String name) {
        Optional<Department> department = Optional.empty();
        if (departments.containsKey(name)) {
            department = Optional.of(departments.get(name));
        }
        return department;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "name='" + name + '\'' +
                ", departments=" + departments +
                '}';
    }
}
