package edu.ntnu.idatt2001.mappe1.hospital.exceptions;

public class RemoveException extends Exception {
    public RemoveException() {
    }

    public RemoveException(String message) {
        super(message);
    }

    public RemoveException(String message, Throwable cause) {
        super(message, cause);
    }

    public RemoveException(Throwable cause) {
        super(cause);
    }
}
