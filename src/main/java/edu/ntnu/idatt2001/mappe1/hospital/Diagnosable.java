package edu.ntnu.idatt2001.mappe1.hospital;

public interface Diagnosable {
    /**
     * Sets the patient's diagnosis.
     * @param diagnosis A non-null diagnosis description.
     *                  Use an empty string to represent no diagnosis.
     */
    void setDiagnosis(String diagnosis);
}
