package edu.ntnu.idatt2001.mappe1.hospital;

public class Employee extends Person {
    /**
     * Creates a new employee.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
