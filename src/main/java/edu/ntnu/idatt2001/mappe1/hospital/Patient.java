package edu.ntnu.idatt2001.mappe1.hospital;

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    /**
     * Creates a new patient.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets the patient's diagnosis.
     * @param diagnosis A non-null diagnosis description.
     *                  Use an empty string to represent no diagnosis.
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        if (diagnosis == null) {
            throw new IllegalArgumentException("diagnosis cannot be null");
        }
        this.diagnosis = diagnosis;
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }
}
