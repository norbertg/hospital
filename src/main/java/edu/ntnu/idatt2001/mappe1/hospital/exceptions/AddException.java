package edu.ntnu.idatt2001.mappe1.hospital.exceptions;

public class AddException extends Exception {
    public AddException() {
    }

    public AddException(String message) {
        super(message);
    }

    public AddException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddException(Throwable cause) {
        super(cause);
    }
}
