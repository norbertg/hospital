package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;
import edu.ntnu.idatt2001.mappe1.hospital.exceptions.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws AddException {
        Hospital hospital = new Hospital("St. Olavs hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);
        Department emergency = hospital.getDepartment("Akutten").orElseThrow();

        System.out.println(hospital);

        try {
            emergency.remove(new Employee("OddEven", "Primtallet", "14130244806"));
        } catch (RemoveException e) {
            System.out.println("Unable to remove OddEven: " + e.getMessage());
        }

        System.out.println(hospital);

        try {
            emergency.remove(new Patient("Sam", "Stone", "84958747674"));
            System.out.println("Sam Stone was removed! This should not happen as he does not exist!");
        } catch (RemoveException e) {
            System.out.println("Unable to remove Sam Stone, as expected.");
        }
        System.out.println(hospital);
    }
}
