package edu.ntnu.idatt2001.mappe1.hospital.employees;

import edu.ntnu.idatt2001.mappe1.hospital.Employee;

public class Nurse extends Employee {
    /**
     * Creates a new nurse.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
