package edu.ntnu.idatt2001.mappe1.hospital.testdoubles;

import edu.ntnu.idatt2001.mappe1.hospital.Person;

public class TestPerson extends Person {
    public TestPerson(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
