package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class HospitalTest {
    @Test
    void constructor_nullName_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new Hospital(null));
    }

    @Test
    void constructor_blankName_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new Hospital(" "));
    }

    @Test
    void constructor_nonBlankName_doesNotThrow() {
        new Hospital("St. Olavs hospital");
    }


    @Test
    void getName_returnsName() {
        Hospital hospital = new Hospital("St. Olavs hospital");

        String name = hospital.getName();

        assertEquals("St. Olavs hospital", name);
    }

    @Test
    void addDepartment_nullDepartment_throwsException() {
        Hospital hospital = new Hospital("St. Olavs hospital");

        assertThrows(IllegalArgumentException.class, () -> hospital.addDepartment(null));
    }

    @Test
    void addDepartment_departmentAlreadyInHospital_throwsException() throws AddException {
        Hospital hospital = new Hospital("St. Olavs hospital");
        Department department = new Department("Neurology");

        hospital.addDepartment(department);
        assertThrows(AddException.class, () -> hospital.addDepartment(department));
    }

    @Test
    void getDepartment_departmentThatDoesNotExist_returnsTheDepartment() {
        Hospital hospital = new Hospital("St. Olavs hospital");

        Optional<Department> gotDepartment = hospital.getDepartment("Neurology");

        assertTrue(gotDepartment.isEmpty());
    }

    @Test
    void getDepartment_departmentThatExists_returnsTheDepartment() throws AddException {
        Hospital hospital = new Hospital("St. Olavs hospital");
        Department department = new Department("Neurology");

        hospital.addDepartment(department);
        Department gotDepartment = hospital.getDepartment("Neurology").get();

        assertEquals(department, gotDepartment);
    }

    @Test
    void getDepartments_emptyHospital_returnsNoDepartments() {
        Hospital hospital = new Hospital("St. Olavs hospital");

        Department[] departments = hospital.getDepartments().toArray(new Department[0]);

        assertArrayEquals(new Department[0], departments);
    }

    @Test
    void getDepartments_filledHospital_returnsNoDepartments() throws AddException {
        Hospital hospital = new Hospital("St. Olavs hospital");
        Department departmentA = new Department("A");
        Department departmentB = new Department("B");

        hospital.addDepartment(departmentA);
        hospital.addDepartment(departmentB);
        Department[] departments = hospital.getDepartments().toArray(new Department[0]);

        assertArrayEquals(new Department[]{ departmentA, departmentB }, departments);
    }
}