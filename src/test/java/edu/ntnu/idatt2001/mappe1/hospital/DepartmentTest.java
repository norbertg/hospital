package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.employees.GeneralPractitioner;
import edu.ntnu.idatt2001.mappe1.hospital.employees.Surgeon;
import edu.ntnu.idatt2001.mappe1.hospital.exceptions.AddException;
import edu.ntnu.idatt2001.mappe1.hospital.exceptions.RemoveException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {
    @Test
    void setName_nullName_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new Department(null));
    }

    @Test
    void setName_blankName_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new Department(" "));
    }

    @Test
    void setName_nonBlankNameAndFilledDepartment_createsCopyOfDepartmentWithDifferentName() throws AddException {
        Department department = new Department("Neurology");
        GeneralPractitioner employee = new GeneralPractitioner(
                "Aaron",
                "Smart",
                "54432665"
        );
        Patient patient = new Patient("John", "Neuroson", "5443277");

        department.addEmployee(employee);
        department.addPatient(patient);
        Department neuro = department.setName("Neuro");
        String neuroName = neuro.getName();
        Patient[] neuroPatients = neuro.getPatients().toArray(new Patient[0]);
        Employee[] neuroEmployees = neuro.getEmployees().toArray(new Employee[0]);

        assertEquals("Neuro", neuroName);
        assertArrayEquals(new Patient[]{ patient }, neuroPatients);
        assertArrayEquals(new Employee[]{ employee }, neuroEmployees);
    }

    @Test
    void getName_returnsName() {
        Department department = new Department("Neurology");

        String name = department.getName();

        assertEquals("Neurology", name);
    }

    @Test
    void addEmployee_nullEmployee_throwsException() {
        Department department = new Department("Neurology");

        assertThrows(IllegalArgumentException.class, () -> department.addEmployee(null));
    }

    @Test
    void getEmployees_filledDepartment_returnsAllEmployees() throws AddException {
        Department department = new Department("Neurology");
        Employee employeeA = new Surgeon("Sara", "Bleu", "54366543");
        Employee employeeB = new GeneralPractitioner("John", "Bleu", "4532566");

        department.addEmployee(employeeA);
        department.addEmployee(employeeB);
        Employee[] employees = department.getEmployees().toArray(new Employee[0]);

        assertArrayEquals(new Employee[] { employeeA, employeeB }, employees);
    }

    @Test
    void addPatient_nullPatient_throwsException() {
        Department department = new Department("Neurology");

        assertThrows(IllegalArgumentException.class, () -> department.addPatient(null));
    }

    @Test
    void getPatients_filledDepartment_returnsAllPatients() throws AddException {
        Department department = new Department("Neurology");
        Patient patientA = new Patient("Sara", "Bleu", "54366543");
        Patient patientB = new Patient("John", "Bleu", "4532566");

        department.addPatient(patientA);
        department.addPatient(patientB);
        Patient[] patients = department.getPatients().toArray(new Patient[0]);

        assertArrayEquals(new Patient[] { patientA, patientB }, patients);
    }

    @Test
    void remove_nullPerson_throwsException() {
        Department department = new Department("Neurology");

        assertThrows(IllegalArgumentException.class, () -> department.remove(null));
    }

    @Test
    void remove_personIsNeitherEmployeeNorPatient_throwsRemoveException() {
        Department department = new Department("Neurology");
        Employee employee = new Surgeon("Sam", "Stone", "544326546");

        assertThrows(RemoveException.class, () -> department.remove(employee));
    }

    @Test
    void remove_personIsPatient_removesPatient() throws AddException, RemoveException {
        Department department = new Department("Neurology");
        Patient patient = new Patient("Sam", "Stone", "544326546");

        department.addPatient(patient);
        department.remove(patient);
        Patient[] patients = department.getPatients().toArray(new Patient[0]);

        assertArrayEquals(new Patient[0], patients);
    }

    @Test
    void remove_personIsEmployee_removesEmployee() throws AddException, RemoveException {
        Department department = new Department("Neurology");
        Surgeon surgeon = new Surgeon("Sam", "Stone", "544326546");

        department.addEmployee(surgeon);
        department.remove(surgeon);
        Employee[] employees = department.getEmployees().toArray(new Employee[0]);
        Patient[] patients = department.getPatients().toArray(new Patient[0]);

        assertArrayEquals(new Employee[0], employees);
    }

    @Test
    void remove_personIsEmployeeAndPatient_removesEmployeeAndPatient() throws AddException, RemoveException {
        Department department = new Department("Neurology");
        Patient patient = new Patient("Sam", "Stone", "544326546");
        Surgeon surgeon = new Surgeon("Sam", "Stone", "544326546");

        department.addPatient(patient);
        department.addEmployee(surgeon);
        department.remove(patient);
        Patient[] patients = department.getPatients().toArray(new Patient[0]);
        Employee[] employees = department.getEmployees().toArray(new Employee[0]);

        assertArrayEquals(new Patient[0], patients);
        assertArrayEquals(new Employee[0], employees);
    }
}