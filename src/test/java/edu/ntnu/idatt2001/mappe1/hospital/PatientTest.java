package edu.ntnu.idatt2001.mappe1.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {
    @Test
    void getDiagnosis_defaultDiagnosis_returnsEmptyString() {
        Patient patient = new Patient("Gary", "Gray", "1000");

        String diagnosis = patient.getDiagnosis();

        assertEquals("", diagnosis);
    }

    @Test
    void setDiagnosis_nullDiagnosis_throwsException() {
        Patient patient = new Patient("Gary", "Gray", "1000");

        assertThrows(IllegalArgumentException.class, () -> patient.setDiagnosis(null));
    }

    @Test
    void setDiagnosis_nonEmptyDiagnosis_setsDiagnosis() {
        Patient patient = new Patient("Gary", "Gray", "1000");

        patient.setDiagnosis("bronchitis");
        String diagnosis = patient.getDiagnosis();

        assertEquals("bronchitis", diagnosis);
    }

    @Test
    void setDiagnosis_emptyDiagnosis_setsDiagnosisToEmpty() {
        Patient patient = new Patient("Gary", "Gray", "1000");

        patient.setDiagnosis("nonempty");
        patient.setDiagnosis("");
        String diagnosis = patient.getDiagnosis();

        assertEquals("", diagnosis);
    }
}