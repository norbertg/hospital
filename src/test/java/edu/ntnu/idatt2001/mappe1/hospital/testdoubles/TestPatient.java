package edu.ntnu.idatt2001.mappe1.hospital.testdoubles;

import edu.ntnu.idatt2001.mappe1.hospital.Patient;

public class TestPatient extends Patient {
    public TestPatient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String getDiagnosis() {
        return super.getDiagnosis();
    }
}
