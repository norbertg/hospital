package edu.ntnu.idatt2001.mappe1.hospital.employees;

import edu.ntnu.idatt2001.mappe1.hospital.testdoubles.TestPatient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SurgeonTest {
    @Test
    void setDiagnosis_nullDiagnosable_throwsException() {
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("John", "Calgary", "754382132");

        assertThrows(IllegalArgumentException.class, () -> generalPractitioner.setDiagnosis(null, "bronchitis"));
    }

    @Test
    void setDiagnosis_nullDiagnosis_throwsException() {
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("John", "Calgary", "754382132");
        TestPatient patient = new TestPatient("Bronn", "Chitis", "544326");

        assertThrows(IllegalArgumentException.class, () -> generalPractitioner.setDiagnosis(patient, null));
    }

    @Test
    void setDiagnosis_patientWithDiagnosis_setsPatientsDiagnosis() {
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("John", "Calgary", "754382132");
        TestPatient patient = new TestPatient("Bronn", "Chitis", "544326");

        generalPractitioner.setDiagnosis(patient, "bronchitis");
        String diagnosis = patient.getDiagnosis();

        assertEquals("bronchitis", diagnosis);
    }
}