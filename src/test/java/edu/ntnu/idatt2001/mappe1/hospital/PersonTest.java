package edu.ntnu.idatt2001.mappe1.hospital;

import edu.ntnu.idatt2001.mappe1.hospital.testdoubles.TestPerson;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    @Test
    void constructor_nullFirstName_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson(null, "Johnson", "4333241")
        );
    }

    @Test
    void constructor_nullLastName_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson("Alison", null, "4333241")
        );
    }

    @Test
    void constructor_nullSocialSecurityNumber_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson("Alison", "Johnson", null)
        );
    }

    @Test
    void constructor_blankFirstName_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson(" ", "Johnson", "4333241")
        );
    }

    @Test
    void constructor_blankLastName_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson("Alison", " ", "4333241")
        );
    }

    @Test
    void constructor_blankSocialSecurityNumber_throwsException() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new TestPerson("Alison", "Johnson", " ")
        );
    }

    @Test
    void constructor_nonBlankNamesAndNonBlankSocialSecurityNumber_doesNotThrowException() {
        new TestPerson("Alison", "Garrison", "4333241");
    }

    @Test
    void getFirstName_returnsFirstName() {
        TestPerson person = new TestPerson("Peter", "Johnson", "4333241");

        String firstName = person.getFirstName();

        assertEquals("Peter", firstName);
    }

    @Test
    void setFirstName_nullName_throwsException() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        assertThrows(IllegalArgumentException.class, () -> person.setFirstName(null));
    }

    @Test
    void setFirstName_blankName_throwsException() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        assertThrows(IllegalArgumentException.class, () -> person.setFirstName(" "));
    }

    @Test
    void setFirstName_nonBlankName_setsFirstName() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        person.setFirstName("Peter");
        String firstName = person.getFirstName();

        assertEquals("Peter", firstName);
    }

    @Test
    void getLastName_returnsTheLastName() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        String lastName = person.getLastName();

        assertEquals("Garrison", lastName);
    }

    @Test
    void setLastName_nullName_throwsException() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        assertThrows(IllegalArgumentException.class, () -> person.setLastName(null));
    }

    @Test
    void setLastName_blankName_throwsException() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        assertThrows(IllegalArgumentException.class, () -> person.setLastName(" "));
    }

    @Test
    void setLastName_nonBlankName_setsTheName() {
        TestPerson person = new TestPerson("Alison", "Garrison", "104329805");

        person.setLastName("Johnson");
        String lastName = person.getLastName();

        assertEquals("Johnson", lastName);
    }

    @Test
    void getFullName_returnsPersonsFullName() {
        TestPerson person = new TestPerson("Alison", "Garrison", "453566");

        String fullName = person.getFullName();

        assertEquals("Alison Garrison", fullName);
    }
}